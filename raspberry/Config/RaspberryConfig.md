# How to configure raspberry

# Install

- dnsmasq
- hostapd
- feh
- imagemagik
- vsftpd
- libgtk-3-dev
- libgtk-3.0
- apache2


## Step 1 : Configure Access Point
In file : /etc/network/interfaces

    # Include files from /etc/network/interfaces.d:
    source-directory /etc/network/interfaces.d

    auto lo
    	iface lo inet loopback

#    auto enxb827eb4b173d
#  	  iface enxb827eb4b173d inet static
#    	address 192.168.0.1
#    	netmask 255.255.255.0

    auto wlan0
    	iface wlan0 inet static
    	address 192.168.1.1
    	netmask 255.255.255.0

In file /etc/hostapd/hostapd.conf

    interface=wlan0
    ssid=MiroirValem
    hw_mode=g
    channel=6
    wmm_enabled=0
    macaddr_acl=0
    auth_algs=1
    ignore_broadcast_ssid=0
    wpa=2
    wpa_passphrase=MiroirValem
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP

In file /etc/dhcpcd.conf (at the end)

    interface wlan0
    	static ip_address=192.168.1.1/24
    	denyinterfaces wlan0
    	denyinterfaces eth0

In file /etc/dnsmasq.conf

      interface=wlan0
      	dhcp-range=192.168.1.11,192.168.1.30,255.255.255.0,24h


-> to connect to a new network, edit file /etc/wpa_supplicant/wpa_supplicant.conf


## Step 2 : Turn the screen

edit the file config.txt
  > sudo nano /boot/config.txt

add the following line
  > display_rotate=3

save the file with CTRL+S and the escape with CTRL+X
and reboot raspberry

  > sudo reboot

Or directly in the raspberry parameters : home>preference>screenconfiguration



## Step 3 : Set up the server
if appache2 is already installed, open 2 terminal, in the first one launch
  > nc -l 1025

In the second one launch
  > nc localhost 1025

Try to write something in the first terminal and see if the second one receives it.

You can do the same by launching in a terminal
  > nc -l 1025

Download the app "Socket Protocol" on you phone, connect your phone to the raspberry network. In the app, select the IP address and the port, then send information and see if they are received.

(Attention : il faut mettre un entré à la fin de la ligne qu'on souhaite envoyer depuis l'application)

## Step 4 : Define the communication.sh script as default script at begining

Edit the autostart file

> sudo nano /etc/xdg/lxsession/LXDE-pi/autostart

Add after the @pcmanfm line

> lxterminal -e "~/Documents/PR/pr_miroir_de_valem/communication.sh"


## Step 5 : Pixelize

Make dans le dossier pixelize
In the raspberry folder
./Pixelize/make_db Drawings/*


## Step 6 : Hide cursor

In /etc/lightdm/lightdm.conf
> xserver-command = X -nocursor


# Test Procedure

go to raspberry/
make all Script's file runable
chmod -R +x Scripts

launch ./Scripts/miroir_de_Valem
If it works, it should take a picture and then creates the mosaic.

To launch the full programm, launch ./communication.sh
