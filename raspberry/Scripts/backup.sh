#!/bin/bash

DEST_FOLDER='/media/pi/PRMIROIR'
DATE=$(date +%Y%m%d_%H%M%S)
DEST_FILE="backup_${DATE}"
BACKUP_CMD='/bin/cp'

echo $DATE
$BACKUP_CMD /home/pi/Documents/PR/pr_miroir_de_valem/raspberry/Pictures -r  $DEST_FOLDER/$DEST_FILE
$BACKUP_CMD /home/pi/Documents/PR/pr_miroir_de_valem/raspberry/CommandeReflet -r  $DEST_FOLDER/$DEST_FILE
$BACKUP_CMD /home/pi/Documents/PR/pr_miroir_de_valem/raspberry/comments.csv $DEST_FOLDER/$DEST_FILE
$BACKUP_CMD /home/pi/Documents/PR/pr_miroir_de_valem/raspberry/commandesDessins.csv $DEST_FOLDER/$DEST_FILE
$BACKUP_CMD /home/pi/Documents/PR/pr_miroir_de_valem/raspberry/commandesReflets.csv $DEST_FOLDER/$DEST_FILE

