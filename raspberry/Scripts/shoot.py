# -*- coding: utf-8 -*-

from PIL import Image
from picamera import PiCamera
from time import sleep

# On récupère un objet de classe PiCamera qui permet d'accéder
# aux fonctionnalités associées
camera = PiCamera() 
camera.rotation=0
camera.preview_fullscreen=True


#resolution max camera = 3280 × 2464 ; resolution ecran = 1920 x 1080.
camera.resolution = (1080, 1920) # on utilise 1080 x 1920 pour un plein ecran lorsque vertical
camera.color_effects = (128,128) #prend une image en noir et blanc
camera.brightness = 70 # luminosité optimale a priori = 60 à nu mais 70 avec film teinté
camera.contrast =45 # contraste optimal a priori= 35
camera.hflip = True
#camera.image_effect = 'film' # le seul effet interessant est 'film' qui va mettre du grain style argentique

camera.start_preview(rotation=90) # image affichée a l'écran
img = Image.open('mask.png')

pad= Image.new('RGBA', (
    ((img.size[0] + 31) // 32)*32,
    ((img.size[1]+15)//16)*16,
))
pad.paste(img,(0,0), img)
o=camera.add_overlay(pad.tobytes(), size=img.size, rotation=90)
o.alpha = 128
o.layer = 3
sleep(1)
img3 = Image.open('mask3.png')

pad= Image.new('RGBA', (
        ((img3.size[0] + 31) // 32)*32,
        ((img3.size[1]+15)//16)*16,
))
pad.paste(img3,(0,0), img3)
o3=camera.add_overlay(pad.tobytes(), size=img3.size, rotation=90)
camera.remove_overlay(o)
o3.alpha = 128
o3.layer = 3

sleep(1)
img2 = Image.open('mask2.png')

pad= Image.new('RGBA', (
        ((img2.size[0] + 31) // 32)*32,
        ((img2.size[1]+15)//16)*16,
))
pad.paste(img2,(0,0), img2)
o2=camera.add_overlay(pad.tobytes(), size=img2.size, rotation=90)
camera.remove_overlay(o3)
o2.alpha = 128
o2.layer = 3


sleep(1)
img1 = Image.open('mask1.png')

pad= Image.new('RGBA', (
        ((img1.size[0] + 31) // 32)*32,
        ((img1.size[1]+15)//16)*16,
))
pad.paste(img1,(0,0), img1)
o1=camera.add_overlay(pad.tobytes(), size=img1.size, rotation=90)
camera.remove_overlay(o2)
o1.alpha = 128
o1.layer = 3

#camera.annotate_text = "Hello world!" # texte écrit sur l'image, inutile pour le moment
sleep(2) # temps avant la capture:
camera.capture('/home/pi/Documents/PR/pr_miroir_de_valem/raspberry/TemporaryFiles/shoot.jpg')
camera.stop_preview()
