#!/bin/bash


# Check if an image is already opened

#check if feh is already open
pid=`pidof -x feh`
#echo "pid = ${pid}"

#open new image over actual image / on the front
feh -F --force-aliasing $1 &

echo "p_ok"

#wait a bit to be sure the new image is opened before closing the actual image
#sleep 5

#if feh was already opened, kill the first instance
if [ -n "${pid}" ];
then
	kill $pid
else
	echo "No first instance"
fi
