# PR Miroir de Valem
 
## Projet : Le miroir de Valem


Le Miroir de Valem est un dispositif physique d'art numérique low-tech, libre, open-source, solidaire, interactif et évolutif.

Sa genèse : Pour s’immerger dans la production de sa série de sculptures « Teranga », Valem, sculpteur, a réalisé, de nombreux dessins inspirés de ses voyages au Sénégal. Préoccupée par la précarité des conditions de vie des personnes avec lesquelles elle a tissé des liens au cours de ses rencontres, elle a souhaité créer ce dispositif solidaire lui permettant de rendre à ces familles le fruit de la générosité dont elle a été nourrie lors de ses voyages.

## Ses objectifs 

• Matérialiser le lien créé par l'art entre les êtres humains quelle que soit leur culture

• Placer le numérique au cœur de la rencontre physique pour susciter le dialogue

• Promouvoir les concepts de frugalité et de partage

• Créer un objet de collaboration entre l'art et la technique impliquant des acteurs aux compétences diverses pour partager des valeurs communes

• Permettre aux familles à l’origine des dessins de bénéficier du retour financier pouvant être généré par le dispositif


## Son fonctionnement 
Le spectateur se place devant le miroir et interagit avec lui via une tablette placée sous le miroir.
Son « reflet » lui est renvoyé sous la forme d’une image en noir et blanc dont chaque pixel est un dessin de Valem (portrait inspiré de ses voyages). 
La tablette lui permet de :

• Zoomer sur son reflet pour voir les dessins qui le composent

• Ajouter son reflet à la galerie des reflets

• Consulter la galerie des reflets et des dessins en grand format

• Acquérir un dessin ou un reflet au format numérique ou papier

## Conventions

### Git

### Commits

Les messages de commit sont en anglais.

 Message de commit: "[{feature du backlog}] [{type}] message de commit"
 type: 

- ADD | FEAT (nouvelle feature)
- FIX (correction)
- IMPR (improvment, refactoring)
- CLEAN (cleaning)
- DOC (documentation, comments)
- TEST (tests)

 Exemple:  [3.5 Envoie de l'apperçu du nouveau reflet à la tablette][FEAT] Connection established
 
### Utilisation des branches

Les branches seront utilisées comme telles :

- `Master` sera la branche de production de la partie logicielle du dispositif. Par conséquent, elle devra toujours être à jour et propre niveau code.
- `Test` sera la branche de pré-production. Elle devra être fonctionnelle ou quasi-fonctionnelle et servira à s'assurer de la validité du code avant de la mettre en production.
- `Raspberry` sera la branche de développement principale des fonctionnalités situées sur la Raspberry.
- `Android` sera la branche de développement principale des fonctionnalités situées sur Android.

Bien entendu, d'autres branches dérivées de celles citées précédemment seront amenées à être créées, mais disparaîtront une fois les développements des fonctionnalités finis.
Ces branches sont donc les seules amenées à rester de manière permanente.
 
### Nommage 

Tout code sera écrit en lowCamelCase (exemple : saveImage)
Pixelize sera en low snake_case (exemple : save_image). Cela est dû au fait qu'il est déjà écrit ainsi et que l'on poursuit cette convention utilisée pour ce logiciel.

### Organisation

Tout le projet se situera sur le même repository, les deux dossiers à la racine serviront à séparer la partie Raspberry de la partie Android :
- /
    - android/
        - bluetooth/
        - interface/
    - raspberry/
        - src/
            - dataBase/
            - results/
            - webcam/
                - shots/
                - scripts/

### Android

#### Gestion du texte

Tous les textes de l'application seront situés dans .../.../strings.xml.
Ceci a pour but de simplifier l'ajout ou la modification de texte dans l'application ainsi que de rendre l'application disponible pour d'autres langues par la suite

### Bluetooth

#### Codes à envoyer pour la communication bluetooth

- `p` : take a photo (and launch pixelize)
- `d - idImage` : display picture (drawing)
- `m - idImage` : display picture (mosaic)
- `z` : zoom
- 


**N.B.** : Ce README est amené à évoluer avec l'avancement du projet. Il revient à ses contributeurs de le modifier en conséquence. 

