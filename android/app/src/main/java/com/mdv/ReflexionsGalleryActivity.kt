package com.mdv

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.mdv.ReflexionsGalleryActivity.ImageGalleryAdapter
import kotlinx.android.synthetic.main.drawings_gallery_activity.*
import kotlinx.android.synthetic.main.drawings_gallery_activity.imageButton34
import kotlinx.android.synthetic.main.drawings_gallery_activity.imageButton40
import kotlinx.android.synthetic.main.drawings_gallery_activity.imageButton42
import kotlinx.android.synthetic.main.reflexions_gallery_activity.*
import java.io.*
import java.net.Socket

class ReflexionsGalleryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.reflexions_gallery_activity)

        val layoutManager = GridLayoutManager(this, 3)
        val recyclerView = findViewById<View>(R.id.rf_images) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        val adapter = ImageGalleryAdapter(this, Reflexions.getReflexions())
        recyclerView.adapter = adapter

        // Home Button
        imageButton42.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Laisser un commentaire
        imageButton34.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton100.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Back Button
        imageButton40.setOnClickListener{
            finish()
        }
    }


    private inner class ImageGalleryAdapter(
        private val mContext: Context,
        private val mReflexions: Array<Reflexions>
    ) : RecyclerView.Adapter<ImageGalleryAdapter.MyViewHolder>() {

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ImageGalleryAdapter.MyViewHolder {

            val context = parent.context
            val inflater = LayoutInflater.from(context)
            val photoView = inflater.inflate(R.layout.item_photo, parent, false)
            return MyViewHolder(photoView)
        }

        override fun onBindViewHolder(holder: ImageGalleryAdapter.MyViewHolder, position: Int) {

            val reflexions = mReflexions[position]
            val imageView = holder.mPhotoImageView
            val path = getExternalFilesDir (Environment.DIRECTORY_DOWNLOADS)

            val fileName = reflexions.title
            val file = File(path, "/$fileName")
            val imageUri = Uri.fromFile(file)

            Glide.with(mContext)
                .load(imageUri)
                .into(imageView)
        }

        override fun getItemCount(): Int {
            return mReflexions.size
        }

        inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
            View.OnClickListener {

            var mPhotoImageView: ImageView

            init {
                mPhotoImageView = itemView.findViewById<View>(R.id.iv_photo) as ImageView
                itemView.setOnClickListener(this)
            }

            override fun onClick(view: View) {

                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val reflexions = mReflexions[position]

                    val tosend = "m|" + reflexions.title
                    sendMessage(tosend)

                    val intent = Intent(mContext, ReflexionsActivity::class.java)
                    intent.putExtra(ReflexionsActivity.EXTRA_SPACE_PHOTO, reflexions)
                    startActivity(intent)
                }
            }
        }
    }
    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
                // Mauvaise réponse
                else if(response != "p_ok" && response != "stop_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                // Retour = p_ok => Pixelize s'est bien déroulé
                handler.post {
                    //val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
                    // startActivity(intent) // TODO quel comportement si l'affichage s'et bien déorulé
                }

                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}

