package com.mdv

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*



class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()


        // Accéder à la galerie des reflets
        imageButton2.setOnClickListener {
            val intent = Intent(this, ReflexionsGalleryActivity::class.java)
           startActivity(intent)
        }

        // Prendre une photo
        imageButton5.setOnClickListener{
            val intent = Intent(this, TakePhotoActivity::class.java)
            startActivity(intent)
            //val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
           //startActivity(intent)
        }

        // Accéder à la galerie des dessins
        imageButton4.setOnClickListener {
            val intent = Intent(this, DrawingsGalleryActivity::class.java)
            startActivity(intent)
        }

        // Laisser un commentaire
        button18.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton25.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
