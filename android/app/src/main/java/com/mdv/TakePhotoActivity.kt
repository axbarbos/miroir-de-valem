package com.mdv

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.p2.*
import kotlinx.android.synthetic.main.propose_to_save_activity.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket


class TakePhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.p2)

        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()
//        actionBar?.isHideOnContentScrollEnabled = true

        imageButton.setOnClickListener{
            setContentView(R.layout.p3)
            val messageToSend = "p"
            sendMessage(messageToSend)
        }

        // Laisser un commentaire
        imageButton7.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton3.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton10.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton12.setOnClickListener{
            finish()
        }
    }

    private val address = "192.168.1.1" // Valeur Raspberry
//    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
                // Mauvaise réponse
                else if(response != "p_ok" && response != "stop_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                // Retour = p_ok => Pixelize s'est bien déroulé
                handler.post {
                    val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
                    startActivity(intent)
                }

                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}
