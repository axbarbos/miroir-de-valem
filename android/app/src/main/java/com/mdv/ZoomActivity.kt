package com.mdv

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.MotionEvent
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.p5.*
import java.io.*
import java.net.Socket
import android.view.View.OnTouchListener
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout


class ZoomActivity : AppCompatActivity() {
    private var mImageView: ImageView? = null
    private var zoomLevel = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.p5)

        mImageView = findViewById<ImageView>(R.id.imageView)
        val reflexions = intent.getParcelableExtra<Reflexions>(ReflexionsActivity.EXTRA_SPACE_PHOTO)
        val path = getExternalFilesDir (Environment.DIRECTORY_DOWNLOADS)

        val fileName = reflexions.title
        val file = File(path, "/$fileName")
        val imageUri = Uri.fromFile(file)

        Glide.with(this)
            .load(imageUri)
            .into(mImageView!!)

        //Zoom more
        plus.setOnClickListener{
            when(zoomLevel) {
                0 -> { rectangle.layoutParams.width = 220
                    rectangle.layoutParams.height = 380
                    rectangle.requestLayout()
                }
                1 -> { rectangle.layoutParams.width = 160
                    rectangle.layoutParams.height = 250
                    rectangle.requestLayout()
                }
                2 -> { rectangle.layoutParams.width = 120
                    rectangle.layoutParams.height = 170
                    rectangle.requestLayout()
                }
            }
            if (zoomLevel in 0..2) {
                sendMessage("z|i")
                this.zoomLevel++
            }
        }

        //Zoom less
        minus.setOnClickListener{
            when(zoomLevel) {
                1 -> { rectangle.layoutParams.width = 344
                    rectangle.layoutParams.height = 585
                    rectangle.requestLayout()
                }
                2 -> { rectangle.layoutParams.width = 220
                    rectangle.layoutParams.height = 380
                    rectangle.requestLayout()
                }
                3 -> { rectangle.layoutParams.width = 160
                    rectangle.layoutParams.height = 250
                    rectangle.requestLayout()
                }
            }
            if (zoomLevel in 1..3) {
                sendMessage("z|o")
                this.zoomLevel--
            }
            val params = rectangle.layoutParams as ConstraintLayout.LayoutParams
            if (params.bottomMargin > 585 - params.height) {
                params.bottomMargin = 585 - params.height
            }
            if (params.topMargin > 585 - params.height) {
                params.topMargin = 585 - params.height
            }
            if (params.rightMargin > 344 - params.width) {
                params.marginEnd = 344 - params.width
                params.rightMargin = 344 - params.width
            }
            if (params.leftMargin > 344 - params.width) {
                params.marginStart = 344 - params.width
                params.leftMargin = 344 - params.width
            }
        }

        //Go up
        up.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) { // if button pressed
                sendMessage("z|m_u_p")
            } else if (event.actionMasked == MotionEvent.ACTION_UP) { // if button released
                sendMessage("z|m_u_r")
            } else if (event.actionMasked == MotionEvent.ACTION_MOVE) {
                val params = rectangle.layoutParams as ConstraintLayout.LayoutParams
                if (params.bottomMargin < 0) {
                    params.bottomMargin = 0
                }
                if (params.topMargin < 0) {
                    params.topMargin = 0
                }
                if (params.topMargin > 585 - params.height) {
                    params.topMargin = 585 - params.height
                }
                if (params.bottomMargin > 585 - params.height) {
                    params.bottomMargin = 585 - params.height
                }
                if (params.topMargin == 0 && params.bottomMargin >= 0 && params.bottomMargin < 585 - params.height) {
                    params.bottomMargin = params.bottomMargin + 2
                    rectangle.requestLayout()

                } else if (params.bottomMargin == 0 && params.topMargin > 0 && params.topMargin <= 585 - params.height) {
                    params.topMargin = params.topMargin - 2
                    rectangle.requestLayout()
                }
            }
            true
        }

        //Go down
        down.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) { // if button pressed
                sendMessage("z|m_d_p")
            } else if (event.actionMasked == MotionEvent.ACTION_UP) { // if button released
                sendMessage("z|m_d_r")
            } else if (event.actionMasked == MotionEvent.ACTION_MOVE) {
                val params = rectangle.layoutParams as ConstraintLayout.LayoutParams
                if (params.bottomMargin < 0) {
                    params.bottomMargin = 0
                }
                if (params.topMargin < 0) {
                    params.topMargin = 0
                }
                if (params.topMargin > 585 - params.height) {
                    params.topMargin = 585 - params.height
                }
                if (params.bottomMargin > 585 - params.height) {
                    params.bottomMargin = 585 - params.height
                }
                if (params.bottomMargin == 0 && params.topMargin >= 0 && params.topMargin < 585 - params.height) {
                    params.topMargin = params.topMargin + 2
                    rectangle.requestLayout()

                } else if (params.topMargin == 0 && params.bottomMargin > 0 && params.bottomMargin <= 585 - params.height) {
                    params.bottomMargin = params.bottomMargin - 2
                    rectangle.requestLayout()
                }
            }
            true
        }

        //Go left
        left.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) { // if button pressed
                sendMessage("z|m_l_p")
            } else if (event.actionMasked == MotionEvent.ACTION_UP) { // if button released
                sendMessage("z|m_l_r")
            } else if (event.actionMasked == MotionEvent.ACTION_MOVE) {
                val params = rectangle.layoutParams as ConstraintLayout.LayoutParams
                if (params.rightMargin < 0) {
                    params.rightMargin = 0
                    params.marginEnd = 0
                }
                if (params.leftMargin < 0) {
                    params.leftMargin = 0
                    params.marginStart = 0
                }
                if (params.leftMargin > 344 - params.width) {
                    params.leftMargin = 344 - params.width
                    params.marginStart = 344 - params.width
                }
                if (params.rightMargin > 344 - params.width) {
                    params.rightMargin = 344 - params.width
                    params.marginEnd = 344 - params.width
                }
                if (params.leftMargin == 0 && params.rightMargin >= 0 && params.rightMargin < 344 - params.width) {
                    params.marginEnd = params.marginEnd + 2
                    params.rightMargin = params.rightMargin + 2
                    rectangle.requestLayout()

                } else if (params.rightMargin == 0 && params.leftMargin > 0 && params.leftMargin <= 344 - params.width) {
                    params.marginStart = params.marginStart - 2
                    params.leftMargin = params.leftMargin - 2
                    rectangle.requestLayout()
                }
            }
            true
        }

        //Go right
        right.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) { // if button pressed
                sendMessage("z|m_r_p")
            } else if (event.actionMasked == MotionEvent.ACTION_UP) { // if button released
                sendMessage("z|m_r_r")
            } else if (event.actionMasked == MotionEvent.ACTION_MOVE) {
                val params = rectangle.layoutParams as ConstraintLayout.LayoutParams
                if (params.rightMargin < 0) {
                    params.rightMargin = 0
                    params.marginEnd = 0
                }
                if (params.leftMargin < 0) {
                    params.leftMargin = 0
                    params.marginStart = 0
                }
                if (params.leftMargin > 344 - params.width) {
                    params.leftMargin = 344 - params.width
                    params.marginStart = 344 - params.width
                }
                if (params.rightMargin > 344 - params.width) {
                    params.rightMargin = 344 - params.width
                    params.marginEnd = 344 - params.width
                }
                if (params.rightMargin == 0 && params.leftMargin >= 0 && params.leftMargin < 344 - params.width) {
                    params.marginStart = params.marginStart + 2
                    params.leftMargin = params.leftMargin + 2
                    rectangle.requestLayout()
                } else if (params.leftMargin == 0 && params.rightMargin > 0 && params.rightMargin <= 344 - params.width) {
                    params.marginEnd = params.marginEnd - 2
                    params.rightMargin = params.rightMargin - 2
                    rectangle.requestLayout()
                }
            }
            true
        }

        // Laisser un commentaire
        imageButton9.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton30.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Back Button
        button9.setOnClickListener{
            while (zoomLevel > 0) {
                sendMessage("z|o")
                zoomLevel--
            }
            finish()
            val intent = Intent(this, ReflexionsActivity::class.java)
            intent.putExtra(ReflexionsActivity.EXTRA_SPACE_PHOTO, reflexions)
            startActivity(intent)
        }
    }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                    //Replace below IP with the IP of that device in which server socket open.
                    //If you change port then change the port number in the server side code also.

                    Log.d("Socket Client", "sendMessage() initiated")
                    Log.d("Socket Client Address", address)
                    Log.d("Socket Client Port", port.toString())
                    Log.d("Socket Client Message", msg)

                    // Initialisation de l'objet socket
                    val s = Socket(address, port)
                    val out = s.getOutputStream()
                    val output = PrintWriter(out)
                    // Envoi de la requête sur socket
                    output.println(msg)
                    output.flush()

                    // On lit la réponse du serveur
//                    val input = BufferedReader(InputStreamReader(s.getInputStream()))
//                    Log.d("Client", "En attente de réponse")
//                    val response = input.readLine()
//                    Log.d("Server Response", response)

                    // Réponse nulle
//                    if(response == null || response.isEmpty() || response == ""){
//                        Log.e("Server Error", "Response null or empty")
//                        throw Exception("NullResponse")
//                    }
                    // Mauvaise réponse
//                    else if(response != "n_ok"){
//                        Log.e("Server Error", "Bad response")
//                        throw Exception("BadResponse")
//                    }
                    // Retour = n_ok => Le commentaire s'est bien envoyé
                    handler.post {
                    }

                    output.close()
                    Log.d("End", "Output closed")

                    out.close()
                    Log.d("End", "Out closed")

                    s.close()
                    Log.d("End", "Socket closed")

                } catch (e: IOException) {
                    e.printStackTrace()
            }
        })
        thread.start()
    }

}
