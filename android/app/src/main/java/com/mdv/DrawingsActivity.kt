package com.mdv

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity

import android.os.Bundle
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Handler
import android.util.Log
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.activity_reflexions.*
import kotlinx.android.synthetic.main.drawings_gallery_activity.*
import kotlinx.android.synthetic.main.drawings_gallery_activity.imageButton40
import kotlinx.android.synthetic.main.drawings_gallery_activity.imageButton42
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket


class DrawingsActivity : AppCompatActivity() {
    private var mImageView: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawings)

        mImageView = findViewById<ImageView>(R.id.image)
        val drawings = intent.getParcelableExtra<Drawings>(EXTRA_SPACE_PHOTO)

        Glide.with(this)
            .load(drawings!!.url)
            .into(mImageView!!)


        // Acquire Button
        button15.setOnClickListener {
            val intent = Intent(this, ChooseSupportActivity::class.java)
            intent.putExtra("pic",drawings.title+".jpg")
            intent.putExtra("name","Drawing")
            Log.d("img",drawings.title+".jpg")
            startActivity(intent)
        }

        // Home Button
        imageButton42.setOnClickListener{
            sendMessage("stop")
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton40.setOnClickListener{
            sendMessage("stop")
            finish()
        }
    }

    companion object {

        val EXTRA_SPACE_PHOTO = "SpacePhotoActivity.SPACE_PHOTO"
    }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
                // Mauvaise réponse
                else if(response != "p_ok" && response != "stop_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                // Retour = p_ok => Pixelize s'est bien déroulé
                handler.post {
                    //val intent = Intent(this, ProposeToSavePhotoActivity::class.java)
                    // startActivity(intent) // TODO quel comportement si l'affichage s'et bien déorulé
                }

                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}