package com.mdv;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Reflexions implements Parcelable {

    private int mUrl;
    private String mTitle;

    public Reflexions(int url, String title) {
        mUrl = url;
        mTitle = title;
    }

    protected Reflexions(Parcel in) {
        mUrl = in.readInt();
        mTitle = in.readString();
    }

    public static final Creator<Reflexions> CREATOR = new Creator<Reflexions>() {
        @Override
        public Reflexions createFromParcel(Parcel in) {
            return new Reflexions(in);
        }

        @Override
        public Reflexions[] newArray(int size) {
            return new Reflexions[size];
        }
    };

    public int getUrl() {
        return mUrl;
    }

    public void setUrl(int url) {
        mUrl = url;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public static  Reflexions[] getReflexions() {

        return new Reflexions[]{
                //new Reflexions(R.drawable.img_01, "img_01"), // TODO MODIFIER LE PATH : ftp://adresseip:21/chemin  ou ftp://192.168.1.1/Documents/PR/pr_miroir_de_valem/raspberry/Drawings/B-10a.jpg ou ftp://pi:raspValem192.168.1.1/Documents/PR/pr_miroir_de_valem/raspberry/Drawings/B-10a.jpg
              //  new Reflexions("Android\data\com.example.myapplication\files\Download", "b_2"),
                new Reflexions(R.drawable.b_3, "pic_0.jpg"),
                new Reflexions(R.drawable.b_4, "pic_1.jpg"),
                new Reflexions(R.drawable.b_5, "pic_2.jpg"),
                new Reflexions(R.drawable.b_6, "pic_3.jpg"),
                new Reflexions(R.drawable.b_6, "pic_4.jpg"),
                new Reflexions(R.drawable.b_6, "pic_5.jpg"),
                new Reflexions(R.drawable.b_6, "pic_6.jpg"),
                new Reflexions(R.drawable.b_6, "pic_7.jpg"),
                new Reflexions(R.drawable.b_6, "pic_8.jpg"),
                new Reflexions(R.drawable.b_6, "pic_9.jpg"),
                new Reflexions(R.drawable.b_6, "pic_10.jpg"),
                new Reflexions(R.drawable.b_6, "pic_11.jpg"),
                new Reflexions(R.drawable.b_6, "pic_12.jpg"),
                new Reflexions(R.drawable.b_6, "pic_13.jpg"),
                new Reflexions(R.drawable.b_6, "pic_14.jpg"),
                new Reflexions(R.drawable.b_6, "pic_15.jpg"),
                new Reflexions(R.drawable.b_6, "pic_16.jpg"),
                new Reflexions(R.drawable.b_6, "pic_17.jpg"),
                new Reflexions(R.drawable.b_6, "pic_18.jpg"),
                new Reflexions(R.drawable.b_7, "pic_19.jpg"),
                new Reflexions(R.drawable.b_8, "pic_20.jpg"),
                new Reflexions(R.drawable.b_6, "pic_21.jpg"),
                new Reflexions(R.drawable.b_6, "pic_22.jpg"),
                new Reflexions(R.drawable.b_6, "pic_23.jpg"),
                new Reflexions(R.drawable.b_7, "pic_24.jpg"),
                new Reflexions(R.drawable.b_8, "pic_25.jpg"),
                new Reflexions(R.drawable.b_6, "pic_26.jpg"),
                new Reflexions(R.drawable.b_6, "pic_27.jpg"),
                new Reflexions(R.drawable.b_6, "pic_28.jpg"),
                new Reflexions(R.drawable.b_7, "pic_29.jpg"),
                new Reflexions(R.drawable.b_8, "pic_30.jpg"),
                new Reflexions(R.drawable.b_6, "pic_31.jpg"),
                new Reflexions(R.drawable.b_6, "pic_32.jpg"),
                new Reflexions(R.drawable.b_6, "pic_33.jpg"),
                new Reflexions(R.drawable.b_7, "pic_34.jpg"),
                new Reflexions(R.drawable.b_8, "pic_35.jpg"),




                /* new Reflexions(R.drawable.b_9, "b_9"),
                new Reflexions(R.drawable.b_10, "b_10"),
                new Reflexions(R.drawable.b_11, "b_11"),
                new Reflexions(R.drawable.b_12, "b_12"),
                new Reflexions(R.drawable.b_13, "b_13"),
                new Reflexions(R.drawable.b_14, "b_14"),
                new Reflexions(R.drawable.b_15, "b_15"),
                new Reflexions(R.drawable.b_16, "b_16"),
                new Reflexions(R.drawable.b_17, "b_17"),
                new Reflexions(R.drawable.b_18, "b_18"),
                new Reflexions(R.drawable.b_19, "n_19"),
                new Reflexions(R.drawable.b_20, "b_20"),
                new Reflexions(R.drawable.b_21, "b_21"),
                new Reflexions(R.drawable.b_22, "b_22"),
                new Reflexions(R.drawable.b_23, "b_23"),
                new Reflexions(R.drawable.b_24, "b_24"),
                new Reflexions(R.drawable.b_25, "b_25"),
                new Reflexions(R.drawable.b_26, "b_26"),
                new Reflexions(R.drawable.b_27, "b_27"),
                new Reflexions(R.drawable.b_28, "b_28"),
                new Reflexions(R.drawable.b_29, "b_29"),
                new Reflexions(R.drawable.b_30, "b_30"),
                new Reflexions(R.drawable.b_31, "b_31"),
                new Reflexions(R.drawable.b_32, "b_32"),
                new Reflexions(R.drawable.b_33, "b_33"),
                new Reflexions(R.drawable.b_34, "b_34"), */



        };
    }

    //Uri myURI = Uri.parse("android.resource://com.example.project/" + R.drawable.myimage);

    public static Uri getUrI(int res){
        return Uri.parse("android.resource://com.example.mdv/" + res);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mUrl);
        parcel.writeString(mTitle);
    }


}


