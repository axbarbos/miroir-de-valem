package com.mdv

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.p7.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Environment
import android.os.Handler
import android.util.Log
import java.io.*
import java.net.Socket


class ConfirmPhotoSavedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.p7)

        val intent_sent = intent
        val name = intent_sent.getStringExtra("name")


        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Remember that you should never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()

        // Suivant
        button11.setOnClickListener{
            if (name=="Acquisition") {
               // val intent2 = Intent(this, ChooseSupportActivity::class.java)
               // intent.putExtra("name","Reflexion")

                val intent = Intent(this, ChooseSupportActivity::class.java)
                intent.putExtra("name","Reflexion")
                startActivity(intent)

            }
            else if (name =="Saving"){
                sendMessage("stop")
                val intent2 = Intent(this, HomeActivity::class.java)
                startActivity(intent2)
                
            }
        }

    }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                Log.d("msgsend",response.substring(0,4))

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
                // Mauvaise réponse
                else if(response != "stop_ok" && response.substring(0,4) != "s_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                handler.post {

                }



                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }
}
