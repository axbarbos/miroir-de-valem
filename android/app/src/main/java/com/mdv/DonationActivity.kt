package com.mdv

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.donation_activity.*

class DonationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.donation_activity)

        val intent_sent = intent
        val paper = intent_sent.getStringExtra("paper")
        val numeric = intent_sent.getStringExtra("numeric")
        Log.d("paper_received",paper)
        Log.d("numeric_received",numeric)

        val showButton = findViewById<Button>(R.id.button23)
        val editText = findViewById<EditText>(R.id.editText3)

        showButton.setOnClickListener {
            val text = editText.text
            val message = text.toString()

            if (paper=="none") {
                val intent = Intent(this , GetEmailActivity::class.java )
                val msg = numeric + "$message|"
                intent.putExtra("numeric",msg)
                intent.putExtra("paper","none")
                Log.d("msg_sent_donation",msg)
                Log.d("msg_sent_donation","none")
                startActivity(intent)
            }
            else if (numeric=="none") {
                val intent = Intent(this , GetInfosActivity::class.java )
                val msg = paper + "$message|"
                intent.putExtra("paper",msg)
                intent.putExtra("numeric","none")
                Log.d("msg_sent_donation",msg)
                Log.d("msg_sent_donation","none")
                startActivity(intent)
            }
            else if(numeric!="none" && paper!="none") {
                val intent = Intent(this , GetEmailActivity::class.java )
                val msg = paper + "$message|"
                val msg2 = numeric + "$message|"
                intent.putExtra("paper",msg)
                intent.putExtra("numeric",msg2)
                Log.d("msg_sent_donation",msg)
                Log.d("msg_sent_donation",msg2)
                startActivity(intent)
            }


        }

        // Laisser un commentaire
        imageButton34.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton41.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton42.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton40.setOnClickListener{
            finish()
        }

    }
}