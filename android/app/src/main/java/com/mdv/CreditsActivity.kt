package com.mdv

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.comment_activity.imageButton27
import kotlinx.android.synthetic.main.comment_activity.imageButton28

class CreditsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.credits_activity)


        //Home Button
        imageButton28.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton27.setOnClickListener{
            finish()
        }
    }
}
