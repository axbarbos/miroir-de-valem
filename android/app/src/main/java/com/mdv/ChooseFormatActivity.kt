package com.mdv

import android.content.Intent
import android.os.Bundle
import android.util.Log

import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.choose_format.*


class ChooseFormatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choose_format)

        val intent_sent = intent
        val papier = intent_sent.getStringExtra("paper")
        val numeric = intent_sent.getStringExtra("numeric")

        button23.setOnClickListener {
            if ((checkBox3.isChecked && checkBox4.isChecked && checkBox5.isChecked) || (!checkBox3.isChecked && !checkBox4.isChecked && !checkBox5.isChecked) ||
                (checkBox3.isChecked && checkBox4.isChecked && !checkBox5.isChecked) || (checkBox3.isChecked && !checkBox4.isChecked && checkBox5.isChecked) || (!checkBox3.isChecked && checkBox4.isChecked && checkBox5.isChecked)
            ) return@setOnClickListener
            else if (checkBox3.isChecked) {
                if (numeric == "none") {
                    val msg = papier + "A2|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg)
                    intent.putExtra("numeric", "none")
                    Log.d("msg_sent", msg)
                    startActivity(intent)
                } else {
                    val msg1 = papier + "A2|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg1)
                    intent.putExtra("numeric", numeric)
                    startActivity(intent)
                }
            } else if (checkBox4.isChecked) {
                if (numeric == "none") {
                    val msg = papier + "A3|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg)
                    intent.putExtra("numeric", "none")
                    Log.d("msg_sent", msg)
                    startActivity(intent)
                } else {
                    val msg1 = papier + "A3|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg1)
                    intent.putExtra("numeric", numeric)
                    startActivity(intent)
                }
            } else if (checkBox5.isChecked) {
                if (numeric == "none") {
                    val msg = papier + "A4|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg)
                    intent.putExtra("numeric", "none")
                    Log.d("msg_sent", msg)
                    startActivity(intent)
                } else {
                    val msg1 = papier + "A4|"
                    val intent = Intent(this, DonationActivity::class.java)
                    intent.putExtra("paper", msg1)
                    intent.putExtra("numeric", numeric)
                    startActivity(intent)
                }

            }


        }


        // Laisser un commentaire
        imageButton34.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton41.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton42.setOnClickListener{
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton40.setOnClickListener{
            finish()
        }
    }
}

