package com.mdv

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log


import kotlinx.android.synthetic.main.choose_support_drawings.*
import kotlinx.android.synthetic.main.choose_support_reflexion.*
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket


class ChooseSupportActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intent_sent = intent
        val sender = intent_sent.getStringExtra("name")
        if (sender=="Reflexion") {
        setContentView(R.layout.choose_support_reflexion)

            R_Papier.setOnClickListener{
                val intent = Intent(this , ChooseFormatActivity::class.java )
                intent.putExtra("paper","c_m|paper|")
                intent.putExtra("numeric","none")
                Log.d("msg_sent_support","c_m|paper|")
                startActivity(intent)
            }

            R_Numerique.setOnClickListener{
                val intent = Intent(this , DonationActivity::class.java )
                intent.putExtra("numeric","c_m|numeric|A2|")
                intent.putExtra("paper","none")
                Log.d("msg_sent_support","c_m|numeric|A2|")
                startActivity(intent)
            }

            R_Lesdeux.setOnClickListener{
                val intent = Intent(this , ChooseFormatActivity::class.java )
                intent.putExtra("numeric","c_m|both|")
                Log.d("msg_sent_support","c_m|both")
                startActivity(intent)
            }

            // Laisser un commentaire
            imageButton26.setOnClickListener {
                val intent = Intent(this, CommentActivity::class.java)
                startActivity(intent)
            }

            // Regarder les informations
            imageButton32.setOnClickListener {
                val intent = Intent(this, CreditsActivity::class.java)
                startActivity(intent)
            }

            // Home Button
            imageButton28.setOnClickListener{
                sendMessage("stop")
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }

            // Back Button
            imageButton27.setOnClickListener{
                finish()
            }
        }

        else if (sender=="Drawing") {
        setContentView(R.layout.choose_support_drawings)

            val img_name = intent_sent.getStringExtra("pic")

            D_Papier.setOnClickListener{
                val intent = Intent(this , DonationActivity::class.java ) // Directement au don car un seul format disponible
                intent.putExtra("paper","c_d|" + "$img_name" + "|paper|A4|")
                intent.putExtra("numeric","none")
                Log.d("msg_sent_support","c_d|" + "$img_name" + "|paper|A4|")
                startActivity(intent)
            }

            D_Numerique.setOnClickListener{
                val intent = Intent(this , DonationActivity::class.java ) //Directement au don car 1 seule résolution dispo
                intent.putExtra("numeric","c_d|" + "$img_name" + "|numeric|A4|")
                intent.putExtra("paper","none")
                Log.d("msg_sent_support","c_d|" + "$img_name" + "|numeric|A4|")
                startActivity(intent)
            }

            D_Lesdeux.setOnClickListener{
                val intent = Intent(this , DonationActivity::class.java ) // directement au don
                intent.putExtra("numeric","c_d|" + "$img_name" + "|both|A4|")
                Log.d("msg_sent_support","c_d|" + "$img_name" + "|numeric|A4|")
                startActivity(intent)
            }

            // Laisser un commentaire
            imageButton3.setOnClickListener {
                val intent = Intent(this, CommentActivity::class.java)
                startActivity(intent)
            }

            // Regarder les informations
            imageButton1.setOnClickListener {
                val intent = Intent(this, CreditsActivity::class.java)
                startActivity(intent)
            }

            // Home Button
            imageButton2.setOnClickListener{
                sendMessage("stop")
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()
            }

            // Back Button
            imageButton4.setOnClickListener{
                finish()
            }
        }
        }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                // On lit la réponse du serveur
                val input = BufferedReader(InputStreamReader(s.getInputStream()))
                Log.d("Client", "En attente de réponse")
                val response = input.readLine()
                Log.d("Server Response", response)

                // Réponse nulle
                if(response == null || response.isEmpty() || response == ""){
                    Log.e("Server Error", "Response null or empty")
                    throw Exception("NullResponse")
                }
//                // Mauvaise réponse
                else if(response != "p_ok" && response != "stop_ok"){
                    Log.e("Server Error", "Bad response")
                    throw Exception("BadResponse")
                }
                handler.post {
                }

                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }


}


