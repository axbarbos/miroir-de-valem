package com.mdv

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.propose_to_save_activity.*
import kotlinx.android.synthetic.main.propose_to_save_activity.button10
import kotlinx.android.synthetic.main.propose_to_save_activity.checkBox
import kotlinx.android.synthetic.main.propose_to_save_activity.checkBox2
import kotlinx.android.synthetic.main.propose_to_save_activity.imageButton19
import kotlinx.android.synthetic.main.propose_to_save_activity.imageButton20
import org.apache.commons.net.ftp.FTP
import org.apache.commons.net.ftp.FTPClient
import java.io.*
import java.net.Socket


class ProposeToSavePhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.propose_to_save_activity)

        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.hide()

        // Take picture again
        button4.setOnClickListener{
            sendMessage("stop")
            val intent = Intent(this, TakePhotoActivity::class.java)
            startActivity(intent)
            finish()
        }

        button10.setOnClickListener {
            if ((checkBox.isChecked && checkBox2.isChecked) || (!checkBox.isChecked && !checkBox2.isChecked) || (checkBox6.isChecked && checkBox7.isChecked) || (!checkBox6.isChecked && !checkBox7.isChecked)) return@setOnClickListener
            else if (checkBox.isChecked) {
                if (checkBox6.isChecked) { // YES - YES Save and acquire the picture
                    val intent = Intent(this, ConfirmPhotoSavedActivity::class.java)
                    intent.putExtra("name","Acquisition")
                    startActivity(intent)
                    sendMessage("s") //save is launched & pic is downloaded into the engine

                }
                else { // YES - NO Save the picture
                    sendMessage("s") //save is launched & pic is downloaded into the engine
                    val intent = Intent(this, ConfirmPhotoSavedActivity::class.java)
                    intent.putExtra("name","Saving")
                    startActivity(intent)
                }
            } else if (checkBox2.isChecked) {
                if (checkBox6.isChecked) {// NO - YES Acquire the picture
                    Log.d("state","not saved but buying")
                    val intent = Intent(this, ChooseSupportActivity::class.java)
                    intent.putExtra("name","Reflexion")
                    startActivity(intent)

                }
                else {
                    sendMessage("stop")
                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }

        // Laisser un commentaire
        imageButton18.setOnClickListener {
            val intent = Intent(this, CommentActivity::class.java)
            startActivity(intent)
        }

        // Regarder les informations
        imageButton31.setOnClickListener {
            val intent = Intent(this, CreditsActivity::class.java)
            startActivity(intent)
        }

        // Home Button
        imageButton20.setOnClickListener{
            sendMessage("stop")
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Back Button
        imageButton19.setOnClickListener{
            // finish()
        }
    }

    private val address = "192.168.1.1" // Valeur Raspberry
    //    private val address = "192.168.43.1" // Valeur Test
    private val port = 1025

    fun sendMessage(msg: String) {
        val handler = Handler()
        val thread = Thread(Runnable {
            try {
                //Replace below IP with the IP of that device in which server socket open.
                //If you change port then change the port number in the server side code also.

                Log.d("Socket Client", "sendMessage() initiated")
                Log.d("Socket Client Address", address)
                Log.d("Socket Client Port", port.toString())
                Log.d("Socket Client Message", msg)

                // Initialisation de l'objet socket
                val s = Socket(address, port)
                val out = s.getOutputStream()
                val output = PrintWriter(out)
                // Envoi de la requête sur socket
                output.println(msg)
                output.flush()

                        // On lit la réponse du serveur
                        val input = BufferedReader(InputStreamReader(s.getInputStream()))
                        Log.d("Client", "En attente de réponse")
                        val response = input.readLine()
                        Log.d("Server Response", response)

                        Log.d("msgsend",response)

                        // Réponse nulle
                        if(response == null || response.isEmpty() || response == ""){
                            Log.e("Server Error", "Response null or empty")
                            throw Exception("NullResponse")
                        }
                        // Sauvegarde
                        else if(response.substring(0,4) == "s_ok") {
                            // Retour = s_ok|nom_du_fichier => la sauvegarde a fonctionné et le fichier s'apelle xxxx
                            handler.post {
                                val fileName = response.substring(5)
                                val path = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
                                val file = File(path, "/$fileName")

                                downloadAndSaveFile(file.absolutePath, fileName)
                            }
                        }
                        // Mauvaise réponse
                        else if(response != "stop_ok"){
                            Log.e("Server Error", "Bad response")
                            throw Exception("BadResponse")
                        }
                        handler.post {

                        }



                output.close()
                Log.d("End", "Output closed")

                out.close()
                Log.d("End", "Out closed")

                s.close()
                Log.d("End", "Socket closed")

            } catch (e: IOException) {
                e.printStackTrace()
            }
        })
        thread.start()
    }

    @Throws(IOException::class)
    fun downloadAndSaveFile(localFile: String ,localname: String ): Boolean? {
        val handler = Handler()
        val thread = Thread(Runnable {
        var ftp: FTPClient? = null
        val user = "pi"
        val password = "raspValem"

        val raspdir = "Documents/PR/pr_miroir_de_valem/raspberry/Preview/"
        val filename = "$raspdir$localname"
            Log.d("chemin",filename)

            try {
                ftp = FTPClient()
                Log.d("1","entre dans la fonction")
                ftp.connectTimeout = 5000
                Log.d("2","set timetout")
                ftp.connect("192.168.1.1", 21)
                Log.d("test", "Connected. Reply: " + ftp.getReplyString());

                ftp.login(user, password)
                Log.d("test2", "Logged in");
                ftp.setFileType(FTP.BINARY_FILE_TYPE)
                Log.d("test3", "Downloading");
                ftp.enterLocalPassiveMode()

                var outputStream: OutputStream? = null
                var success = false
                try {
                    outputStream = BufferedOutputStream(
                        FileOutputStream(
                            localFile
                        )
                    )
                    success = ftp.retrieveFile(filename, outputStream)
                } finally {
                    outputStream?.close()
                }

                handler.post {
                    Log.d("4","succes")
                }

            } finally {
                if (ftp != null) {
                    //ftp.logout()
                    //ftp.disconnect()
                }
            }

        })
        thread.start()
    return true }
}
